package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import businessLayer.Restaurant;

public class RestaurantSerializator implements Serializable {
	private final static String fileName = "C:\\Users\\georg\\Desktop\\PT2019\\PT2019_30423_Marian_Georgiana\\pt2019_30423_marian_georgiana_assignment_4/restaurant.ser";

	/*
	 * Writes restaurant object to desired file
	 * 
	 */
	public static void serializeRestaurant(Restaurant r) {

		try {
			FileOutputStream fileOut = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(r);
			out.close();
			fileOut.close();
		} catch (Exception e) {

			System.out.println(e.getMessage() + "aici" + e.getClass());
		}
	}

	/**
	 * Reads the restaurant object from the given file.
	 * 
	 * @return Returns the restaurant object read from the serialized file.
	 */
	public static Restaurant deserializeRestaurant() {

		Restaurant r = new Restaurant();
		try {

			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			r = (Restaurant) in.readObject();
			in.close();
			fileIn.close();
		} catch (Exception e) {

			System.out.println(e.getMessage());
		}
		return r;
	}
}
