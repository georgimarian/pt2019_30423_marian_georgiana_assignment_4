package dataLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class FileWriter {
	private static int billNumber;

	public static void generateBill(Order order, Restaurant restaurant) {
		billNumber++;
		try {
			PrintWriter out = new PrintWriter("bill" + billNumber + ".txt");
			out.println("**********************************");
			out.println("BILL SUMMARY");
			out.println("**********************************\n\n");
			out.println(" ");
			out.println("Bill number " + billNumber);
			out.println("Table " + order.getTable() + " has bought ");
			for (MenuItem m : restaurant.getOrderedProducts().get(order)) {
				out.println(m.toString() + "\n");
			}
			out.println("........................TOTAL    " + restaurant.computeOrderPrice(order) + " lei");
			out.close();
			System.out.println("Bill for order " + billNumber + " has been generated");
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
	}
}
