package businessLayer;

import java.io.Serializable;
import java.time.LocalDate;

public class Order implements Serializable {
	private static int count;
	private int OrderID;
	private int table;
	private LocalDate Date;

	public Order() {
		count++;
		this.OrderID = count;
	}

	public int getOrderID() {
		return OrderID;
	}

	public void setOrderID(int orderID) {
		OrderID = orderID;
	}

	public LocalDate getDate() {
		return Date;
	}

	public void setDate(LocalDate date) {
		Date = LocalDate.now();
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	@Override
	public int hashCode() {
		return OrderID;

	}

	@Override
	public boolean equals(Object o) {
		return this == o;
		// if(!(o instanceof Order))
	}
	
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("id: " + OrderID + " table: " + table);
		return stringBuilder.toString();
	}

}
