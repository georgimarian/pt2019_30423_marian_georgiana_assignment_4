package businessLayer;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BaseProduct extends MenuItem implements Serializable{
	public BaseProduct(String name, int price) {
		super(name);
		this.price = price;
	}

	private int price;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public float computePrice() {
		return price;
	}

}
