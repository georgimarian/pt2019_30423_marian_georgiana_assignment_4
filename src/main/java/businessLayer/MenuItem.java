package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable{
	private static int count = 0;
	private int id;
	private String name;

	public MenuItem(String name) {
		count++;
		this.id = count;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("id: " + id + " " + name);
		return stringBuilder.toString();
	}

	public abstract float computePrice();

}
