package businessLayer;

import java.util.HashSet;
import java.util.Set;

public class CompositeProduct extends MenuItem {
	private HashSet<MenuItem> components;

	public CompositeProduct(String name, Set<MenuItem> components) {
		super(name);
		this.components = new HashSet<MenuItem>();
		this.components = (HashSet<MenuItem>) components;
	}

	@Override
	public float computePrice() {
		int totalPrice = 0;
		for (MenuItem p : components) {
			totalPrice += p.computePrice();
		}
		return totalPrice;
	}

	public Set<MenuItem> getComponents() {
		return components;
	}

	public void setComponents(Set<MenuItem> components) {
		this.components = (HashSet<MenuItem>) components;
	}

	public void addComponent(MenuItem m) {
		components.add(m);
	}

	public void removeComponent(MenuItem m) {
		components.remove(m);
	}

}
