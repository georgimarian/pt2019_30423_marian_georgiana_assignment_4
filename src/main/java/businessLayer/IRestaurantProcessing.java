package businessLayer;

import java.util.Set;

import presentationLayer.ChefGraphicalUserInterface;

public interface IRestaurantProcessing {
	/**
	 * Methods for the administrator of the system
	 */
	/**
	 * 
	 * @param name
	 * @param components
	 */
	public void createNewMenuItem(String name, Set<MenuItem> components);

	/**
	 * 
	 * @param m
	 */
	public void deleteMenuItem(MenuItem m);

	/**
	 * Edits an item in the menu
	 * 
	 * @param m - item to be edited
	 * @param n - item containing information to update
	 * 
	 */
	public void editMenuItem(MenuItem m, MenuItem n);

	/**
	 * Methods for the waiter
	 */

	/**
	 * Creates a new entry in a hashmap, having an Order as key and a Set<MenuItems>
	 * as value
	 * 
	 * @param o
	 * @param products
	 * @pre o.getOrderID() > 0
	 */
	public void createNewOrder(Order o, Set<MenuItem> products);

	/**
	 * Computes price for an order
	 * 
	 * @param o
	 * @return returns the computed price
	 * @pre o.getOrderID() > 0
	 * @post @nochange
	 */
	public float computeOrderPrice(Order o);

	/**
	 * Generates bill for an order
	 * 
	 * @param o - given order
	 * @pre o.getOrderID() > 0
	 * @post @nochange
	 */
	public void generateBill(Order o);

	public void observerAdd(ChefGraphicalUserInterface cgui);
}
