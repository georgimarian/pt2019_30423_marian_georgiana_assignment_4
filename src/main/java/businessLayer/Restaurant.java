package businessLayer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import dataLayer.FileWriter;
import presentationLayer.ChefGraphicalUserInterface;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

	private HashMap<Order, HashSet<MenuItem>> orderedProducts;
	private HashSet<MenuItem> menu;

	public Restaurant() {
		orderedProducts = new HashMap<Order, HashSet<MenuItem>>();
		menu = new HashSet<MenuItem>();
	}

	public HashMap<Order, HashSet<MenuItem>> getOrderedProducts() {
		return orderedProducts;
	}

	public void setOrderedProducts(Map<Order, HashSet<MenuItem>> menu) {
		this.orderedProducts = (HashMap<Order, HashSet<MenuItem>>) menu;
	}

	public HashSet<MenuItem> getMenu() {
		return menu;
	}

	/**
	 * Adds new menu item to menu
	 * 
	 * @pre m.getId() > 0
	 *
	 */
	public void addMenuItem(MenuItem m) {
		assert m.getId() > 0;
		menu.add(m);
	}

	/**
	 * @post menu.contains(m) == true
	 */
	public void createNewMenuItem(String name, Set<MenuItem> components) {
		if (components.size() == 1) {
			Iterator<MenuItem> it = components.iterator();
			MenuItem m = it.next();
			assert m.getId() > 0;
			menu.add(m);
			assert menu.contains(m) == true;
		} else {
			CompositeProduct m = new CompositeProduct(name, components);
			assert m.getId() > 0;
			menu.add(m);
			assert menu.contains(m) == true;
		}
	}

	/**
	 * Deletes an entry from the hash set
	 * 
	 * @param m - the deleted item
	 * @pre menu.contains(m) == true
	 * @post menu.contains(m) == false
	 * @invariant isWellFormed()
	 */
	public void deleteMenuItem(MenuItem m) {
		assert menu.contains(m) == true;
		menu.remove(m);
		assert menu.contains(m) == false;

	}

	/**
	 * Edits a menu item from the hash set
	 * 
	 * @pre menu.contains(m) == true
	 * @post menu.contains(m) == false
	 * @invariant isWellFormed()
	 */
	public void editMenuItem(MenuItem m, MenuItem n) {
		assert menu.contains(m) == true;
		menu.remove(m);
		menu.add(n);
		assert menu.contains(m) == false;
	}

	/**
	 * @post orderedProducts.get(o) != null
	 * @invariant isWellFormed()
	 */
	public void createNewOrder(Order o, Set<MenuItem> products) {
		assert o.getOrderID() > 0;

		orderedProducts.put(o, (HashSet<MenuItem>) products);
		setChanged();
		notifyObservers();
		
		assert orderedProducts.get(o) != null;
	}

	public float computeOrderPrice(Order o) {
		assert o.getOrderID() > 0;
		float finalPrice = 0;
		HashSet<MenuItem> items = orderedProducts.get(o);
		for (MenuItem m : items) {
			finalPrice += m.computePrice();
		}
		return finalPrice;
	}

	public void generateBill(Order o) {
		assert o.getOrderID() > 0;
		FileWriter.generateBill(o, this);
	}

	/**
	 * Well formed method
	 * 
	 * @return
	 */
	protected boolean isWellFormed() {
		Set<MenuItem> allMenuItems = new HashSet<MenuItem>();
		int i = 0;
		for (Order currentOrder : orderedProducts.keySet()) {
			for (MenuItem currentItem : orderedProducts.get(currentOrder)) {
				allMenuItems.add(currentItem);
				i++;
			}
		}
		return allMenuItems.size() == i;
	}

	public void observerAdd(ChefGraphicalUserInterface cgui) {
		this.addObserver(cgui);	
	}

}
