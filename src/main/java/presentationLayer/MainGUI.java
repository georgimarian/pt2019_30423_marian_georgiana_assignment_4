package presentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;

public class MainGUI extends JFrame {
	JButton btnAdminSection;
	JButton btnChefSection;
	JButton btnWaiterSection;

	public MainGUI() {
		getContentPane().setBackground(new Color(255, 248, 220));
		setFont(new Font("Bitstream Charter", Font.PLAIN, 14));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Restaurant Management");
		getContentPane().setLayout(null);

		btnAdminSection = new JButton("Admin Section");
		btnAdminSection.setFont(new Font("Vivaldi", Font.BOLD, 25));
		btnAdminSection.setBackground(new Color(216, 191, 216));
		btnAdminSection.setBounds(233, 72, 267, 84);
		getContentPane().add(btnAdminSection);

		btnChefSection = new JButton("Chef Section");
		btnChefSection.setFont(new Font("Vivaldi", Font.BOLD, 25));
		btnChefSection.setBackground(new Color(173, 255, 47));
		btnChefSection.setBounds(233, 191, 267, 84);
		getContentPane().add(btnChefSection);

		btnWaiterSection = new JButton("Waiter Section");
		btnWaiterSection.setFont(new Font("Vivaldi", Font.BOLD, 25));
		btnWaiterSection.setBackground(new Color(175, 238, 238));
		btnWaiterSection.setBounds(233, 311, 267, 84);
		getContentPane().add(btnWaiterSection);

		JLabel lblWelcomeToThe = new JLabel("Welcome to the Restaurant Management Application!");
		lblWelcomeToThe.setFont(new Font("Vivaldi", Font.BOLD, 25));
		lblWelcomeToThe.setBounds(100, 25, 544, 34);
		getContentPane().add(lblWelcomeToThe);
		this.setBounds(100, 100, 750, 600);
	}

	public void addBtnAdminSectionActionListener(final ActionListener actionListener) {
		btnAdminSection.addActionListener(actionListener);
	}

	public void addBtnWaiterSectionActionListener(final ActionListener actionListener) {
		btnWaiterSection.addActionListener(actionListener);
	}
	
	public void addBtnChefSectionActionListener(final ActionListener actionListener) {
		btnChefSection.addActionListener(actionListener);
	}
}
