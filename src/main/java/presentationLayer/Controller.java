package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class Controller {
	private MainGUI gui;
	private AdministratorGraphicalUserInterface agui;
	private WaiterGraphicalUserInterface wgui;
	private ChefGraphicalUserInterface cgui;

	public Controller(MainGUI gui, AdministratorGraphicalUserInterface agui, WaiterGraphicalUserInterface wgui,
			ChefGraphicalUserInterface cgui) {
		this.gui = gui;
		this.agui = agui;
		this.wgui = wgui;
		this.cgui = cgui;
	}

	public void start() {
		gui.setLocationRelativeTo(null);
		gui.setVisible(true);

		initializeButtonListeners();
	}

	public void refreshAguiTable(Restaurant restaurant) {
		agui.showTable(restaurant);
		agui.updateMenuComboBox(agui.getCreateMenuComboBox());
		agui.updateMenuComboBox(agui.getDeleteMenuComboBox());
		agui.updateMenuComboBox(agui.getEditMenuComboBox());
		wgui.updateMenuComboBox(wgui.getMenuComboBox());
		RestaurantSerializator.serializeRestaurant(restaurant);
		agui.getContentPane().revalidate();
		agui.getContentPane().repaint();
		wgui.getContentPane().revalidate();
		wgui.getContentPane().repaint();
	}

	public void initializeButtonListeners() {
		gui.addBtnAdminSectionActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agui.setLocationRelativeTo(null);
				agui.setVisible(true);
				gui.dispose();
			}
		});
		gui.addBtnWaiterSectionActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.dispose();
				wgui.setLocationRelativeTo(null);
				wgui.setVisible(true);
			}
		});

		gui.addBtnChefSectionActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.dispose();
				cgui.setLocationRelativeTo(null);
				cgui.setVisible(true);
			}
		});
		wgui.addBtnReturnToHomeActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wgui.dispose();
				gui.setLocationRelativeTo(null);
				gui.setVisible(true);
			}
		});

		final HashSet<MenuItem> orderedItems = new HashSet<MenuItem>();
		/**
		 * Methods for adding and deleting product to order
		 */
		wgui.addAddButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MenuItem m = wgui.getMenuItemComboBox();
					wgui.appendToOrderedTextArea(m.toString());
					orderedItems.add(m);
				} catch (Exception e1) {

				}
			}
		});

		wgui.addDelButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				MenuItem m = wgui.getMenuItemComboBox();
				orderedItems.remove(m);
				wgui.setOrderedTextAreaText("");
				for (MenuItem n : orderedItems) {
					wgui.appendToOrderedTextArea(n.toString());
				}
				wgui.getContentPane().revalidate();
				wgui.getContentPane().repaint();
				}catch(Exception e1) {
					
				}
			}
		});

		wgui.addBtnCreateNewOrderActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int table = Integer.parseInt(wgui.getTableTextField());
					Order order = new Order();
					order.setTable(table);
					Restaurant r = (Restaurant) wgui.getRestaurant();
					r.createNewOrder(order, orderedItems);
					RestaurantSerializator.serializeRestaurant(r);
					wgui.updateTextArea(order);
					wgui.updateOrderComboBox(wgui.getComputeComboBox());
					wgui.updateOrderComboBox(wgui.getBillComboBox());
					cgui.updateTextArea(order.toString() + r.getOrderedProducts().get(order).toString());
					wgui.getContentPane().revalidate();
					wgui.getContentPane().repaint();
				} catch (Exception e1) {
					// wgui.displayMessage(e1.toString());
				}
			}
		});

		wgui.addBtnComputePriceActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				Order o = wgui.getOrderToComputePriceComboBox();
				Restaurant r = (Restaurant) wgui.getRestaurant();
				float price = r.computeOrderPrice(o);
				RestaurantSerializator.serializeRestaurant(r);
				wgui.setOrderPriceTextField((int) price);
				wgui.getContentPane().revalidate();
				wgui.getContentPane().repaint();
				}catch(Exception e1) {
					
				}
			}
		});

		wgui.addBtnGenerateBillActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				Order o = wgui.getOrderToBillComboBox();
				Restaurant r = (Restaurant) wgui.getRestaurant();
				RestaurantSerializator.serializeRestaurant(r);
				r.generateBill(o);
				}catch(Exception e1) {
					
				}
			}
		});

		agui.addBtnReturnToHomeActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agui.dispose();
				gui.setLocationRelativeTo(null);
				gui.setVisible(true);
			}
		});

		agui.addBtnCreateSimpleMenuItemActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String name = agui.getSimpleNameTextField();
					String price = agui.getCreateSimplePriceTextField();
					if (name.equals("")) {
						throw new Exception("please enter a name!");
					}
					if (price.equals("")) {
						throw new Exception("Please enter a price!");
					}
					HashSet<MenuItem> element = new HashSet<MenuItem>();
					MenuItem item = new BaseProduct(name, Integer.parseInt(price));
					element.add(item);
					Restaurant restaurant = (Restaurant) agui.getRestaurant();
					restaurant.createNewMenuItem(name, element);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshAguiTable(restaurant);
				} catch (Exception e1) {
					agui.displayMessage(e1.toString());
				}
			}
		});

		final HashSet<MenuItem> components = new HashSet<MenuItem>();
		agui.addAddBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MenuItem m = agui.getCreateComboBoxProduct();
					agui.appendToTextArea(m);
					components.add(m);
					agui.getContentPane().revalidate();
					agui.getContentPane().repaint();
				} catch (Exception e1) {

				}
			}
		});
		agui.addBtnCreateMenuItemActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String name = agui.getCreateNameTextField();
					if (name.equals("")) {
						throw new Exception("Please enter a name!");
					}
					Restaurant restaurant = (Restaurant) agui.getRestaurant();
					restaurant.createNewMenuItem(name, components);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshAguiTable(restaurant);
				} catch (Exception e1) {
					agui.displayMessage(e1.toString());
				}
			}
		});

		agui.addBtnDeleteMenuItemActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MenuItem m = agui.getDeleteProductComboBox();
					Restaurant restaurant = (Restaurant) agui.getRestaurant();
					restaurant.deleteMenuItem(m);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshAguiTable(restaurant);
				} catch (Exception e1) {

				}
			}
		});
		/**
		 * @TODO edit composite product
		 */
		agui.addBtnEditMenuItemActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MenuItem m = agui.getEditProductComboBox();
					String newName = agui.getEditNameTextField();
					int newPrice = agui.getEditPriceTextField();
					MenuItem n = new BaseProduct(newName, newPrice);
					n.setId(m.getId());
					Restaurant restaurant = (Restaurant) agui.getRestaurant();
					restaurant.editMenuItem(m, n);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshAguiTable(restaurant);
				} catch (Exception e1) {

				}
			}
		});
		
		cgui.addBtnReturnToHomeActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cgui.dispose();
				gui.setLocationRelativeTo(null);
				gui.setVisible(true);
			}
		});

	}
}
