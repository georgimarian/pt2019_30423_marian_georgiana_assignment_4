package presentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.TextArea;
import java.awt.Button;

public class WaiterGraphicalUserInterface extends JFrame {
	private JTextArea textArea;
	private JButton btnGenerateBill;
	private JButton btnComputePrice;
	private JButton btnCreateNewOrder;
	private JButton btnReturnToHome;
	private JButton btnAdd;
	private JButton btnDel;
	private TextArea productsOrdered;
	private JComboBox<MenuItem> addToOrderComboBox;
	private JComboBox<Order> costComboBox;
	private JComboBox<Order> generateBillComboBox;
	private List<Order> currentOrders = new ArrayList<Order>();
	private JTextField orderPriceTextField;
	private IRestaurantProcessing restaurant;
	private JLabel lblAddTable;
	private JTextField tableTextField;

	public WaiterGraphicalUserInterface(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
		getContentPane().setBackground(new Color(240, 255, 255));
		setFont(new Font("Bitstream Charter", Font.PLAIN, 14));
		setTitle("Waiter section");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		btnGenerateBill = new JButton("Generate Bill");
		btnGenerateBill.setFont(new Font("Script MT Bold", Font.PLAIN, 25));
		btnGenerateBill.setBackground(new Color(245, 245, 245));
		btnGenerateBill.setBounds(90, 423, 231, 56);
		getContentPane().add(btnGenerateBill);

		btnComputePrice = new JButton("Compute Price");
		btnComputePrice.setFont(new Font("Script MT Bold", Font.BOLD, 25));
		btnComputePrice.setBackground(new Color(250, 235, 215));
		btnComputePrice.setBounds(90, 314, 202, 39);
		getContentPane().add(btnComputePrice);

		btnCreateNewOrder = new JButton("Create New Order");
		btnCreateNewOrder.setFont(new Font("Script MT Bold", Font.PLAIN, 25));
		btnCreateNewOrder.setBackground(new Color(230, 230, 250));
		btnCreateNewOrder.setBounds(90, 201, 231, 39);
		getContentPane().add(btnCreateNewOrder);

		btnReturnToHome = new JButton("Return to Home");
		btnReturnToHome.setFont(new Font("Script MT Bold", Font.PLAIN, 25));
		btnReturnToHome.setBackground(new Color(240, 255, 240));
		btnReturnToHome.setBounds(466, 484, 231, 56);
		getContentPane().add(btnReturnToHome);

		textArea = new JTextArea();
		textArea.setBounds(466, 98, 243, 347);
		textArea.setWrapStyleWord(true);
		this.setBounds(100, 100, 750, 600);

		JScrollPane scrollPane = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(466, 98, 243, 347);
		getContentPane().add(scrollPane);

		JLabel lblActiveTables = new JLabel("Active tables");
		lblActiveTables.setFont(new Font("Script MT Bold", Font.PLAIN, 20));
		lblActiveTables.setHorizontalAlignment(SwingConstants.LEFT);
		lblActiveTables.setBounds(466, 50, 129, 16);
		getContentPane().add(lblActiveTables);

		addToOrderComboBox = new JComboBox<MenuItem>();
		addToOrderComboBox.setBounds(90, 50, 231, 22);
		for (MenuItem m : ((Restaurant) restaurant).getMenu()) {
			addToOrderComboBox.addItem(m);
		}
		getContentPane().add(addToOrderComboBox);

		JLabel lblAddProductsTo = new JLabel("Add products to order");
		lblAddProductsTo.setFont(new Font("Script MT Bold", Font.PLAIN, 18));
		lblAddProductsTo.setBounds(90, 26, 192, 16);
		getContentPane().add(lblAddProductsTo);

		generateBillComboBox = new JComboBox<Order>();
		generateBillComboBox.setBounds(90, 388, 231, 22);
		for (Order m : ((Restaurant) restaurant).getOrderedProducts().keySet()) {
			generateBillComboBox.addItem(m);
		}
		getContentPane().add(generateBillComboBox);

		JLabel lblCheckout = new JLabel("Checkout");
		lblCheckout.setFont(new Font("Script MT Bold", Font.PLAIN, 18));
		lblCheckout.setBounds(90, 366, 192, 16);
		getContentPane().add(lblCheckout);

		JLabel lblSeeCost = new JLabel("See cost");
		lblSeeCost.setFont(new Font("Script MT Bold", Font.PLAIN, 18));
		lblSeeCost.setBounds(90, 253, 192, 16);
		getContentPane().add(lblSeeCost);

		costComboBox = new JComboBox<Order>();
		costComboBox.setBounds(90, 271, 231, 22);
		for (Order m : ((Restaurant) restaurant).getOrderedProducts().keySet()) {
			costComboBox.addItem(m);
		}
		getContentPane().add(costComboBox);

		orderPriceTextField = new JTextField();
		orderPriceTextField.setBounds(304, 326, 53, 22);
		getContentPane().add(orderPriceTextField);
		orderPriceTextField.setColumns(10);

		productsOrdered = new TextArea();
		productsOrdered.setBounds(90, 78, 231, 93);
		getContentPane().add(productsOrdered);

		btnAdd = new JButton("add");
		btnAdd.setBackground(new Color(255, 250, 250));
		btnAdd.setFont(new Font("Script MT Bold", Font.PLAIN, 11));
		btnAdd.setBounds(328, 48, 53, 25);
		getContentPane().add(btnAdd);

		btnDel = new JButton("del");
		btnDel.setBackground(new Color(255, 250, 250));
		btnDel.setFont(new Font("Script MT Bold", Font.PLAIN, 13));
		btnDel.setBounds(327, 78, 53, 25);
		getContentPane().add(btnDel);
		
		lblAddTable = new JLabel("Add table");
		lblAddTable.setFont(new Font("Script MT Bold", Font.PLAIN, 18));
		lblAddTable.setBounds(90, 177, 91, 16);
		getContentPane().add(lblAddTable);
		
		tableTextField = new JTextField();
		tableTextField.setColumns(10);
		tableTextField.setBounds(175, 177, 53, 22);
		getContentPane().add(tableTextField);
	}

	public void addBtnCreateNewOrderActionListener(final ActionListener actionListener) {
		btnCreateNewOrder.addActionListener(actionListener);
	}

	public void addBtnComputePriceActionListener(final ActionListener actionListener) {
		btnComputePrice.addActionListener(actionListener);
	}

	public void addBtnGenerateBillActionListener(final ActionListener actionListener) {
		btnGenerateBill.addActionListener(actionListener);
	}

	public void addBtnReturnToHomeActionListener(final ActionListener actionListener) {
		btnReturnToHome.addActionListener(actionListener);
	}

	public void addAddButtonActionListener(final ActionListener actionListener) {
		btnAdd.addActionListener(actionListener);
	}

	public void addDelButtonActionListener(final ActionListener actionListener) {
		btnDel.addActionListener(actionListener);
	}

	public MenuItem getMenuItemComboBox() {
		return (MenuItem) addToOrderComboBox.getSelectedItem();
	}

	public JComboBox<MenuItem> getMenuComboBox() {
		return addToOrderComboBox;
	}

	public Order getOrderToBillComboBox() {
		return (Order) generateBillComboBox.getSelectedItem();
	}
	
	public JComboBox<Order> getBillComboBox() {
		return generateBillComboBox;
	}

	public Order getOrderToComputePriceComboBox() {
		return (Order) costComboBox.getSelectedItem();
	}
	
	public JComboBox<Order> getComputeComboBox() {
		return costComboBox;
	}

	public void appendToOrderedTextArea(String s) {
		productsOrdered.append("\n" + s);
	}

	public String getOrderedTextAreaText() {
		return productsOrdered.getText();
	}
	
	public String getTableTextField() {
		return tableTextField.getText();
	}

	public void setOrderedTextAreaText(String s) {
		productsOrdered.setText(s);
	}

	public void setOrderPriceTextField(int price) {
		orderPriceTextField.setText("" + price);
	}

	public void updateTextArea(Order o) {
		currentOrders.add(o);
		textArea.append("\n" + o.toString());
	}

	public void updateMenuComboBox(JComboBox<MenuItem> comboBox) {
		comboBox.removeAllItems();
		for (MenuItem c : ((Restaurant) restaurant).getMenu()) {
			comboBox.addItem(c);
		}
		getContentPane().add(comboBox);
		getContentPane().revalidate();
		getContentPane().repaint();
	}

	public void updateOrderComboBox(JComboBox<Order> comboBox) {
		comboBox.removeAllItems();
		for (Order c : ((Restaurant) restaurant).getOrderedProducts().keySet()) {
			comboBox.addItem(c);
		}
		getContentPane().add(comboBox);
		getContentPane().revalidate();
		getContentPane().repaint();
	}
	
	public IRestaurantProcessing getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
	}
	
	public void displayMessage(final String messageText) {
		JOptionPane.showMessageDialog(this, messageText);
	}
}
