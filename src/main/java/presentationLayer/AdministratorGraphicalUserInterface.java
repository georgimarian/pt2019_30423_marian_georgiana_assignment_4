package presentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.TextArea;

public class AdministratorGraphicalUserInterface extends JFrame {
	private JButton btnCreateMenuItem;
	private JButton btnCreateSimpleItem;
	private JButton btnDeleteMenuItem;
	private JButton btnEditMenuItem;
	private JButton btnReturnToHome;
	private JButton btnAdd;
	private JTable table;
	private IRestaurantProcessing restaurant;
	private JTextField createNameTextField;
	private JTextField createSimpleName;
	private JTextField createSimplePrice;
	private JTextField editName;
	private JTextField editPrice;
	private TextArea textArea;

	private JComboBox<MenuItem> createComboBox;
	private JComboBox<MenuItem> deleteComboBox;
	private JComboBox<MenuItem> editComboBox;

	public AdministratorGraphicalUserInterface(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
		getContentPane().setBackground(new Color(255, 228, 196));
		setFont(new Font("Bitstream Charter", Font.PLAIN, 14));
		setTitle("Admin Section");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		this.setBounds(100, 100, 750, 600);

		btnCreateMenuItem = new JButton("Create Menu Item");
		btnCreateMenuItem.setFont(new Font("Script MT Bold", Font.BOLD, 21));
		btnCreateMenuItem.setBackground(new Color(224, 255, 255));
		btnCreateMenuItem.setBounds(47, 204, 217, 35);
		getContentPane().add(btnCreateMenuItem);

		btnDeleteMenuItem = new JButton("Delete Menu Item");
		btnDeleteMenuItem.setFont(new Font("Script MT Bold", Font.BOLD, 21));
		btnDeleteMenuItem.setBackground(new Color(216, 191, 216));
		btnDeleteMenuItem.setBounds(450, 425, 217, 40);
		getContentPane().add(btnDeleteMenuItem);

		btnEditMenuItem = new JButton("Edit Menu Item");
		btnEditMenuItem.setFont(new Font("Script MT Bold", Font.BOLD, 21));
		btnEditMenuItem.setBackground(new Color(255, 255, 224));
		btnEditMenuItem.setBounds(47, 513, 217, 31);
		getContentPane().add(btnEditMenuItem);

		btnReturnToHome = new JButton("Return To Home");
		btnReturnToHome.setFont(new Font("Script MT Bold", Font.BOLD, 21));
		btnReturnToHome.setBackground(new Color(240, 255, 240));
		btnReturnToHome.setBounds(450, 488, 217, 40);
		getContentPane().add(btnReturnToHome);

		table = new JTable();
		table.setFont(new Font("Source Sans Pro Semibold", Font.PLAIN, 13));
		table.setBackground(new Color(255, 228, 225));
		showTable((Restaurant) restaurant);
		table.setBounds(437, 353, 222, -262);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(450, 67, 270, 264);
		getContentPane().add(scrollPane);

		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setFont(new Font("Script MT Bold", Font.BOLD, 25));
		lblMenu.setBounds(554, 23, 97, 31);
		getContentPane().add(lblMenu);

		createComboBox = new JComboBox<MenuItem>();
		deleteComboBox = new JComboBox<MenuItem>();
		deleteComboBox.setBounds(450, 385, 203, 27);
		editComboBox = new JComboBox<MenuItem>();
		editComboBox.setBounds(47, 425, 217, 27);

		for (MenuItem m : ((Restaurant) restaurant).getMenu()) {
			createComboBox.addItem(m);
			deleteComboBox.addItem(m);
			editComboBox.addItem(m);
		}
		createComboBox.setBounds(108, 67, 150, 22);
		getContentPane().add(createComboBox);
		getContentPane().add(deleteComboBox);
		getContentPane().add(editComboBox);

		JLabel lblCreateNewItem = new JLabel("Create new Composite Item");
		lblCreateNewItem.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblCreateNewItem.setBounds(61, 23, 197, 15);
		getContentPane().add(lblCreateNewItem);

		textArea = new TextArea();
		textArea.setBounds(35, 95, 288, 103);
		getContentPane().add(textArea);

		JLabel lblDeleteMenuItem = new JLabel("Delete Menu Item");
		lblDeleteMenuItem.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblDeleteMenuItem.setBounds(450, 357, 135, 15);
		getContentPane().add(lblDeleteMenuItem);

		JLabel lblEditMenuItem = new JLabel("Edit Menu Item");
		lblEditMenuItem.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblEditMenuItem.setBounds(47, 394, 135, 15);
		getContentPane().add(lblEditMenuItem);

		createNameTextField = new JTextField();
		createNameTextField.setBounds(108, 42, 150, 22);
		getContentPane().add(createNameTextField);
		createNameTextField.setColumns(10);

		JLabel lblName = new JLabel("name");
		lblName.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblName.setBounds(35, 49, 35, 15);
		getContentPane().add(lblName);

		JLabel lblContents = new JLabel("contents");
		lblContents.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblContents.setBounds(35, 70, 61, 15);
		getContentPane().add(lblContents);

		btnCreateSimpleItem = new JButton("Create Simple Item");
		btnCreateSimpleItem.setFont(new Font("Script MT Bold", Font.BOLD, 21));
		btnCreateSimpleItem.setBackground(new Color(224, 255, 255));
		btnCreateSimpleItem.setBounds(47, 346, 217, 35);
		getContentPane().add(btnCreateSimpleItem);

		JLabel lblCreateNewSimple = new JLabel("Create new Simple Item");
		lblCreateNewSimple.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblCreateNewSimple.setBounds(57, 261, 197, 15);
		getContentPane().add(lblCreateNewSimple);

		createSimpleName = new JTextField();
		createSimpleName.setBounds(104, 291, 150, 22);
		getContentPane().add(createSimpleName);
		createSimpleName.setColumns(10);

		JLabel label = new JLabel("name");
		label.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		label.setBounds(35, 294, 35, 15);
		getContentPane().add(label);

		JLabel lblPrice = new JLabel("price");
		lblPrice.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblPrice.setBounds(35, 322, 35, 15);
		getContentPane().add(lblPrice);

		createSimplePrice = new JTextField();
		createSimplePrice.setColumns(10);
		createSimplePrice.setBounds(104, 319, 150, 22);
		getContentPane().add(createSimplePrice);

		editName = new JTextField();
		editName.setColumns(10);
		editName.setBounds(104, 460, 150, 22);
		getContentPane().add(editName);

		JLabel label_1 = new JLabel("name");
		label_1.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		label_1.setBounds(47, 463, 35, 15);
		getContentPane().add(label_1);

		editPrice = new JTextField();
		editPrice.setColumns(10);
		editPrice.setBounds(104, 488, 150, 22);
		getContentPane().add(editPrice);

		JLabel label_2 = new JLabel("price");
		label_2.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		label_2.setBounds(47, 481, 35, 15);
		getContentPane().add(label_2);

		btnAdd = new JButton("add");
		btnAdd.setBackground(new Color(248, 248, 255));
		btnAdd.setFont(new Font("Script MT Bold", Font.PLAIN, 11));
		btnAdd.setBounds(270, 63, 53, 25);
		getContentPane().add(btnAdd);

	}

	public void addBtnCreateMenuItemActionListener(final ActionListener actionListener) {
		btnCreateMenuItem.addActionListener(actionListener);
	}

	public void addBtnCreateSimpleMenuItemActionListener(final ActionListener actionListener) {
		btnCreateSimpleItem.addActionListener(actionListener);
	}

	public void addBtnDeleteMenuItemActionListener(final ActionListener actionListener) {
		btnDeleteMenuItem.addActionListener(actionListener);
	}

	public void addBtnEditMenuItemActionListener(final ActionListener actionListener) {
		btnEditMenuItem.addActionListener(actionListener);
	}

	public void addBtnReturnToHomeActionListener(final ActionListener actionListener) {
		btnReturnToHome.addActionListener(actionListener);
	}

	public void addAddBtnActionListener(final ActionListener actionListener) {
		btnAdd.addActionListener(actionListener);
	}

	public String getCreateNameTextField() {
		return createNameTextField.getText();
	}

	public String getSimpleNameTextField() {
		return createSimpleName.getText();
	}

	public String getCreateSimplePriceTextField() {
		return createSimplePrice.getText();
	}

	public String getEditNameTextField() {
		return editName.getText();
	}

	public int getEditPriceTextField() {
		return Integer.parseInt(editPrice.getText());
	}

	public JComboBox<MenuItem> getCreateMenuComboBox() {
		return createComboBox;
	}

	public MenuItem getCreateComboBoxProduct() {
		return (MenuItem) createComboBox.getSelectedItem();
	}

	public JComboBox<MenuItem> getDeleteMenuComboBox() {
		return deleteComboBox;
	}

	public JComboBox<MenuItem> getEditMenuComboBox() {
		return editComboBox;
	}

	public MenuItem getDeleteProductComboBox() {
		return (MenuItem) deleteComboBox.getSelectedItem();
	}

	public MenuItem getEditProductComboBox() {
		return (MenuItem) editComboBox.getSelectedItem();
	}

	public void appendToTextArea(MenuItem m) {
		textArea.append("\n" + m.toString());
	}

	public void updateMenuComboBox(JComboBox<MenuItem> comboBox) {
		comboBox.removeAllItems();
		for (MenuItem c : ((Restaurant) restaurant).getMenu()) {
			comboBox.addItem(c);
		}
		getContentPane().add(comboBox);
		getContentPane().revalidate();
		getContentPane().repaint();
	}

	public void showTable(Restaurant restaurant) {

		table.setModel(
				new DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null },
						{ null, null, null }, { null, null, null }, }, new String[] { "id", "name", "price" }));

		HashSet<MenuItem> menu = restaurant.getMenu();

		DefaultTableModel model = new DefaultTableModel();

		Object[] columnsName = new Object[3];
		columnsName[0] = "id";
		columnsName[1] = "name";
		columnsName[2] = "price";

		model.setColumnIdentifiers(columnsName);

		Object[] rowData = new Object[3];

		for (MenuItem o : menu) {
			rowData[0] = o.getId();
			rowData[1] = o.getName();
			rowData[2] = o.computePrice();
			model.addRow(rowData);
		}
		table.setModel(model);

	}

	public IRestaurantProcessing getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
	}

	public void displayMessage(final String messageText) {
		JOptionPane.showMessageDialog(this, messageText);
	}

}
