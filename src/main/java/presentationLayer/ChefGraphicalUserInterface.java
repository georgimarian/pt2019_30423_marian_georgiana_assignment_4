package presentationLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import businessLayer.IRestaurantProcessing;
import dataLayer.RestaurantSerializator;
import javax.swing.JLabel;

public class ChefGraphicalUserInterface extends JFrame implements Observer {
	private IRestaurantProcessing restaurant;
	private JButton btnReturnToHome;
	private JTextArea textArea;
	private JLabel lblToCook;

	public ChefGraphicalUserInterface(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
		setFont(new Font("Bitstream Charter", Font.PLAIN, 14));
		setTitle("Chef Section");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		this.setBounds(100, 100, 750, 600);

		btnReturnToHome = new JButton("Return to Home");
		btnReturnToHome.setFont(new Font("Script MT Bold", Font.PLAIN, 25));
		btnReturnToHome.setBackground(new Color(240, 255, 240));
		btnReturnToHome.setBounds(41, 465, 231, 56);
		getContentPane().add(btnReturnToHome);

		textArea = new JTextArea();
		textArea.setBounds(466, 98, 243, 347);
		textArea.setWrapStyleWord(true);
		this.setBounds(100, 100, 750, 600);
		JScrollPane scrollPane = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(466, 98, 243, 347);
		getContentPane().add(scrollPane);
		
		lblToCook = new JLabel("To cook: ");
		lblToCook.setFont(new Font("Script MT Bold", Font.PLAIN, 25));
		lblToCook.setBounds(469, 54, 125, 31);
		getContentPane().add(lblToCook);
		

	}

	public void addBtnReturnToHomeActionListener(final ActionListener actionListener) {
		btnReturnToHome.addActionListener(actionListener);
	}
	
	public void updateTextArea(String s) {
		textArea.append("\n" + s);
	}

	public void update(Observable arg0, Object arg1) {
		System.out.println("Chef is an observer!");
	}

	public IRestaurantProcessing getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
	}
}
