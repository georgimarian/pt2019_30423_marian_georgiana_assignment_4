import businessLayer.IRestaurantProcessing;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;
import presentationLayer.AdministratorGraphicalUserInterface;
import presentationLayer.ChefGraphicalUserInterface;
import presentationLayer.Controller;
import presentationLayer.MainGUI;
import presentationLayer.WaiterGraphicalUserInterface;

public class Application {

	public static void main(String[] args) {
		IRestaurantProcessing restaurant = RestaurantSerializator.deserializeRestaurant();
		MainGUI gui = new MainGUI();
		AdministratorGraphicalUserInterface agui = new AdministratorGraphicalUserInterface(restaurant);
		WaiterGraphicalUserInterface wgui = new WaiterGraphicalUserInterface(restaurant);
		ChefGraphicalUserInterface cgui = new ChefGraphicalUserInterface(restaurant);
		restaurant.observerAdd(cgui);
		Controller controller = new Controller(gui, agui, wgui, cgui);
		controller.start();

	}

}
